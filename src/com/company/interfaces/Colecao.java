package com.company.interfaces;

public interface Colecao<T> {
    /**
     * Informa a quantidade de elementos da coleção.
     * @return quantidade de elementos.
     */
    int qtdeElems();

    /**
     * Informa se coleção está vazia ou não.
     * @return verdadeiro se estiver vázia, se não, falso.
     */
    default boolean isVazia() {
        return qtdeElems() == 0;
    }
}
