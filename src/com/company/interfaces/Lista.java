package com.company.interfaces;

import java.util.Iterator;
import java.util.Optional;

public interface Lista<T> extends Colecao<T>, Iterable<T> {

    /**
     * Informa o elemento de determinado índice da lista.
     * @param indice indice do elemento que será acessado.
     * @return elemento contido no índice desejado.
     */
    T acessar(int indice);

    /**
     * Insere um elemento em um determinado índice da lista.
     * @param indice índice em que o elemento será inserido.
     * @param obj elemento que será inserido.
     */
    void inserir(int indice, T obj);

    /**
     * Remove um elemento de um determinado índice da lista.
     * @param indice índice do elemento que será removido.
     * @return elemento removido.
     */
    T remover(int indice);

    /**
     * Pesquisa determido elemento na lista.
     * @param obj elemento que será pesquisado.
     * @return elemento pesquisado, caso seja encontrado.
     */
    Optional<Integer> pesquisar(T obj);

    /**
     * Verifica se determinado elemento está contido na lista.
     * @param obj elemento que será verificado.
     * @return verdadeiro caso o elemento esteja contido, se não, falso.
     */
    default boolean contem(T obj) {
        return pesquisar(obj).isPresent();
    }

    /**
     * Retorna um novo Iterator de IteradorLista.
     */
    @Override
    default Iterator<T> iterator() {
        return new IteradorLista<>(this);
    }

    class IteradorLista<T> implements Iterator<T> {
        private int idxProxElem = 0;
        private Lista<T> lista;

        /**
         * Construtor da classe IteradorLista.
         * @param listaAIterar lista a ser usada.
         */
        IteradorLista(Lista<T> listaAIterar) {
            this.lista = listaAIterar;
        }

        /**
         * Verifica se há um próximo elemento na lista.
         * @return verdadeiro se houver próximo, se não houver, falso.
         */
        @Override
        public boolean hasNext() {
            return idxProxElem < lista.qtdeElems();
        }

        /**
         * Informa o próximo elemento contido na lista.
         * @return próximo elemento contido na lista.
         */
        @Override
        public T next() {
            T next = lista.acessar(idxProxElem);
            idxProxElem++;
            return next;
        }
    }
}
