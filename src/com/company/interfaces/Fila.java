package com.company.interfaces;

// FIFO: first in, first out
public interface Fila<T> extends Colecao<T> {

    /**
     * Enfileira um elemento, colocando-o no fim da fila.
     * @param obj elemnento que será enfileirado.
     */
    void enfileirar(T obj);

    /**
     * Desinfilera o primeiro elemento da fila.
     * @return elemento desinfileirado.
     */
    T desenfileirar();

    /**
     * Informa o primeiro elemento da fila.
     * @return primeiro elemento da fila.
     */
    T olharPrimeiro();
}
