package com.company.interfaces;

// LIFO: last in, first out.
public interface Pilha<T> extends Colecao<T> {
    /**
     * Empilha um objeto na pilha, colocando-o no topo.
     * @param obj elemento a ser empilhado.
     */
    void empilhar(T obj);

    /**
     * Desempilha o primeiro elemento da pilha.
     * @return elemento que foi desimpilhado.
     */
    T desempilhar();

    /**
     * Informa o primeiro elemento da pilha, que está no topo.
     * @return elemento que está no topo.
     */
    T olharTopo();
}
