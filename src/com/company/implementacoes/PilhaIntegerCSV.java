package com.company.implementacoes;

import com.company.interfaces.Pilha;

public class PilhaIntegerCSV implements Pilha<Integer> {

    private String pilhaCSV = "";
    private String topo = "";
    private int qtdElems;

    @Override
    public void empilhar(Integer obj) {
        if (!this.pilhaCSV.isEmpty()) {
            this.pilhaCSV += ", ";
        }
        this.topo = String.valueOf(obj);
        this.pilhaCSV += String.valueOf(obj);
        this.qtdElems++;
    }

    @Override
    public Integer desempilhar() {
        olharTopo();
        String topoDesimpilhado = this.topo;
        this.topo = null;
        this.pilhaCSV = pilhaCSV.substring(0, pilhaCSV.lastIndexOf(","));
        this.topo = pilhaCSV.substring(pilhaCSV.lastIndexOf(" ") + 1, pilhaCSV.length());
        this.qtdElems--;
        return Integer.valueOf(topoDesimpilhado);
    }

    @Override
    public Integer olharTopo() {
        return Integer.valueOf(this.topo);
    }

    public String printPilha() {
        return this.pilhaCSV;
    }

    // Métodos da Coleção:

    @Override
    public int qtdeElems() {
        return this.qtdElems;
    }

    @Override
    public boolean isVazia() {
        return this.topo == null;
    }
}
