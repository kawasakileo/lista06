package com.company.implementacoes;

import com.company.interfaces.Fila;

public class FilaIntegerCSV implements Fila<Integer> {

    private String filaCSV = "";
    private String primeiroElemento = "";
    private int qtdeElems;

    @Override
    public void enfileirar(Integer obj) {
        if (!this.filaCSV.isEmpty()) {
            this.filaCSV += ", ";
            this.filaCSV += String.valueOf(obj);
        } else {
            this.primeiroElemento = String.valueOf(obj);
            this.filaCSV += primeiroElemento;
        }
        qtdeElems++;
    }

    @Override
    public Integer desenfileirar() {
        String elementoDesinfileirado = this.primeiroElemento;
        this.filaCSV = filaCSV.substring(filaCSV.indexOf(", ") + 2, filaCSV.length());
        this.primeiroElemento = this.filaCSV.substring(0, filaCSV.indexOf(", "));
        this.qtdeElems--;
        return Integer.valueOf(elementoDesinfileirado);
    }

    @Override
    public Integer olharPrimeiro() {
        return Integer.valueOf(primeiroElemento);
    }

    public String getFilaCSV() {
        return this.filaCSV;
    }

    // Métodos da Coleção:

    @Override
    public int qtdeElems() {
        return qtdeElems;
    }

    @Override
    public boolean isVazia() {
        return primeiroElemento.isEmpty();
    }
}
