package com.company.implementacoes;

import com.company.interfaces.Lista;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ListaIntegerCSV implements Lista<Integer> {
    private List<String> listaCSV = new ArrayList<>();
    private int qtdeElemens = 0;

    /**
     * O método a seguir faz o acesso ao indíce da lista solicitado
     *
     * @param indice inidice a ser verificado
     * @return o valor existente na lista no indice.
     */
    @Override
    public Integer acessar(int indice) {
        if(qtdeElemens < indice) {
            System.out.println("O índice não existe ainda na lista");
            return 0;
        }
        return Integer.valueOf(listaCSV.get(indice));
    }

    /**
     * O método a seguir faz a insercao de um elemento na lista
     * mais especificamente no indice informado.
     *
     * @param indice indice onde o objeto será inserido
     * @param obj    objeto a ser inserido
     */
    @Override
    public void inserir(int indice, Integer obj) {
        if(isVazia()){
            listaCSV.add(String.valueOf(obj));
        }
        listaCSV.add(indice, String.valueOf(obj));
        qtdeElemens++;
    }

    /**
     * O método a seguir faz a remoção do indice solicitado
     *
     * @param indice indice solicitado a ser removido
     * @return o indice que foi removido
     */
    @Override
    public Integer remover(int indice) {
        if (qtdeElemens < indice) {
            System.out.println("O índice não existe ainda na lista");
            return 0;
        }
        Integer elementoRemovido = Integer.valueOf(listaCSV.get(indice));
        listaCSV.remove(indice);
        qtdeElemens--;
        return elementoRemovido;
    }

    /**
     * O método a seguir faz a pesquisa de forma sequencial
     * do objeto na lista
     *
     * @param obj objeto a ser pesquisado
     * @return se exitir o objeto e seu indice são retornados, se não existir
     * retorna informação de não existencia.
     */
    @Override
    public Optional<Integer> pesquisar(Integer obj) {
        String aux = String.valueOf(obj);
        for(int i = 0; i < listaCSV.size(); i++){
            if (aux.equals(listaCSV.get(i))){
                // System.out.println("Objeto encontrado na posicao: " + listaCSV.get(i));
                return Optional.of(Integer.valueOf(listaCSV.get(i)));
            }
        }
        // System.out.println("Objeto não encontrado");
        return Optional.empty();
    }

    /**
     * O método a seguir faz a verificacao de quantos elementos
     * existem na colecao.
     *
     * @return retorna a quantidade em inteiro, referente ao tamanho da lista de elementos.
     */
    @Override
    public int qtdeElems() {
        return qtdeElemens;
    }

    /**
     * O método a seguir verifica se a lista apresentada é vazia.
     *
     * @return verdadeiro ou falso, dependendo da condicao.
     */
    @Override
    public boolean isVazia() {
        return listaCSV == null;
    }

    public String printLista() {
        if (isVazia()) {
            return "Lista Vazia";
        }
        return listaCSV.toString();
    }
}
