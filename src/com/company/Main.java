package com.company;

import com.company.implementacoes.FilaIntegerCSV;
import com.company.implementacoes.ListaIntegerCSV;
import com.company.implementacoes.PilhaIntegerCSV;

public class Main {

    public static void main(String[] args) {
        System.out.println("Teste da pilha: ");
        testaPilha();
        System.out.println();
        System.out.println("---------------------------------------------------------");

        System.out.println("Testa da fila: ");
        testaFila();
        System.out.println();
        System.out.println("---------------------------------------------------------");

        System.out.println("Teste da lista: ");
        testaLista();
        System.out.println();
        System.out.println("---------------------------------------------------------");
    }

    private static void testaPilha() {
        PilhaIntegerCSV pilhaIntegerCSV = new PilhaIntegerCSV();

        pilhaIntegerCSV.empilhar(7);
        pilhaIntegerCSV.empilhar(15);
        pilhaIntegerCSV.empilhar(9);
        pilhaIntegerCSV.empilhar(2);
        pilhaIntegerCSV.empilhar(11);

        System.out.println("Pilha: " + pilhaIntegerCSV.printPilha());
        System.out.println("Topo: " + pilhaIntegerCSV.olharTopo());
        System.out.println("Desimpilhou: " + pilhaIntegerCSV.desempilhar());
        System.out.println("Novo topo: " + pilhaIntegerCSV.olharTopo());
        System.out.println("Quantidade de elementos: " + pilhaIntegerCSV.qtdeElems());
        System.out.println("Está vazia: " + pilhaIntegerCSV.isVazia());
        System.out.println();
        System.out.println("Pilha: " + pilhaIntegerCSV.printPilha());
        System.out.println("Topo: " + pilhaIntegerCSV.olharTopo());
        System.out.println("Desimpilhou: " + pilhaIntegerCSV.desempilhar());
        System.out.println("Novo topo: " + pilhaIntegerCSV.olharTopo());
        System.out.println("Quantidade de elementos: " + pilhaIntegerCSV.qtdeElems());
        System.out.println("Está vazia: " + pilhaIntegerCSV.isVazia());
        System.out.println();
        System.out.println("Pilha: " + pilhaIntegerCSV.printPilha());
        System.out.println("Topo: " + pilhaIntegerCSV.olharTopo());
        System.out.println("Desimpilhou: " + pilhaIntegerCSV.desempilhar());
        System.out.println("Novo topo: " + pilhaIntegerCSV.olharTopo());
        System.out.println("Quantidade de elementos: " + pilhaIntegerCSV.qtdeElems());
        System.out.println("Está vazia: " + pilhaIntegerCSV.isVazia());

        System.out.println("Pilha: " + pilhaIntegerCSV.printPilha());
    }

    private static void testaFila() {
        FilaIntegerCSV filaIntegerCSV = new FilaIntegerCSV();

        filaIntegerCSV.enfileirar(1000);
        filaIntegerCSV.enfileirar(7000);
        filaIntegerCSV.enfileirar(2000);
        filaIntegerCSV.enfileirar(3000);
        filaIntegerCSV.enfileirar(6000);

        System.out.println("Fila em ordem: " + filaIntegerCSV.getFilaCSV());
        System.out.println("Primeiro: " + filaIntegerCSV.olharPrimeiro());
        System.out.println("Desenfileirou: " + filaIntegerCSV.desenfileirar());
        System.out.println("Novo primeiro: " + filaIntegerCSV.olharPrimeiro());
        System.out.println("Quantidade elementos: " + filaIntegerCSV.qtdeElems());
        System.out.println("Está vazia: " + filaIntegerCSV.isVazia());
        System.out.println();
        System.out.println("Fila em ordem: " + filaIntegerCSV.getFilaCSV());
        System.out.println("Primeiro: " + filaIntegerCSV.olharPrimeiro());
        System.out.println("Desenfileirou: " + filaIntegerCSV.desenfileirar());
        System.out.println("Novo primeiro: " + filaIntegerCSV.olharPrimeiro());
        System.out.println("Quantidade elementos: " + filaIntegerCSV.qtdeElems());
        System.out.println("Está vazia: " + filaIntegerCSV.isVazia());
        System.out.println();
        System.out.println("Fila em ordem: " + filaIntegerCSV.getFilaCSV());
        System.out.println("Primeiro: " + filaIntegerCSV.olharPrimeiro());
        System.out.println("Desenfileirou: " + filaIntegerCSV.desenfileirar());
        System.out.println("Novo primeiro: " + filaIntegerCSV.olharPrimeiro());
        System.out.println("Quantidade elementos: " + filaIntegerCSV.qtdeElems());
        System.out.println("Está vazia: " + filaIntegerCSV.isVazia());

        System.out.println("Fila em ordem: " + filaIntegerCSV.getFilaCSV());
    }

    private static void testaLista() {
        ListaIntegerCSV listaIntegerCSV = new ListaIntegerCSV();

        listaIntegerCSV.inserir(0, 5);
        listaIntegerCSV.inserir(1, 10);
        listaIntegerCSV.inserir(2, 15);
        listaIntegerCSV.inserir(3, 20);
        listaIntegerCSV.inserir(4, 25);

        System.out.println("Lista: " + listaIntegerCSV.printLista());
        System.out.println("Acessou índice 1: " + listaIntegerCSV.acessar(1));
        System.out.println("Pesquisou o 50: " + listaIntegerCSV.pesquisar(50));
        System.out.println("Contém o 5: " + listaIntegerCSV.contem(5));
        System.out.println("Removeu índice 4: " + listaIntegerCSV.remover(4));
        System.out.println("Removeu índice 0: " + listaIntegerCSV.remover(0));

        listaIntegerCSV.inserir(1, 500);
        listaIntegerCSV.inserir(2, 300);
        System.out.println("Inseriu no índice 1: 500");
        System.out.println("Inseriu no índice 2: 300");
        System.out.println(listaIntegerCSV.printLista());
        System.out.println("Está vazia: " + listaIntegerCSV.isVazia());
    }
}
